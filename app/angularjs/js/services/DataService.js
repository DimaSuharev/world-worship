angular.module("skiniaWorshipApp", ["ngCookies"])
	.factory("DataService", function ($http) {
		var query = function(config, callback, scope) {
			$http.post("/DataService", config).then(function (response) {
				callback.call(scope, response.data);
			}, scope.errorHandler);
		};
		/**
		 * Пример селекта
		 * DataService.Select({
         *     columns: ["*"],
         *     schemaName: "Event",
         *     filters: [{columnName: "Id", value: iventId}]
         * }, function(Ivent) {
         * }, scope);
		 */
		var Select = function(config, callback, scope) {
			query(config, callback, scope);
		};
		/**
		 * Пример вставки
		 * DataService.Insert({
         *     columns: [
         *         {columnName: "Name", value: "Test insert"},
         *         {columnName: "Date", value: "2016-10-19"}
         *     ],
         *     schemaName: "Event"
         * }, function(SongFolder) {
         * }, scope);
		 */
		var Insert = function(config, callback, scope) {
			config.operation = "insert";
			query(config, callback, scope);
		};

		var Delete = function(config, callback, scope) {
			config.operation = "delete";
			query(config, callback, scope);
		};

		var Update = function(config, callback, scope) {
			config.operation = "update";
			query(config, callback, scope);
		};

		var GetEntitySchema = function(schemaName, callback, scope) {
			var schema = scope.schemas && scope.schemas[schemaName];
			if (!schema) {
				try {
					var config = {
						operation: "getEntity",
						entityName: schemaName
					};
					query(config, function (entitySchema) {
						scope.schemas[schemaName] = entitySchema;
						callback.call(scope, entitySchema);
					}, scope);
				}
				catch(e) {
					scope.showInformationDialog("Схема:" + entityName + " - не найдена");
				}
			} else {
				callback.call(scope, schema);
			}

		};

		var convertDateToString = function (date) {
			var month = '' + (date.getMonth() + 1),
				day = '' + date.getDate(),
				year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
			return [year, month, day].join('-');
		};

		return {
			Select: Select,
			Insert: Insert,
			Delete: Delete,
			Update: Update,
			GetEntitySchema: GetEntitySchema,
			ConvertDateToString: convertDateToString
		};
	})
;
