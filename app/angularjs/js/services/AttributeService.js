angular.module("skiniaWorshipApp", ["ngCookies"])
	.factory("AttributeService", function ($location, DataService) {
		var getElement = function (element, isAll) {
			return document[isAll ? "querySelectorAll" : "querySelector"](element);
		};

		var getAttribute = function (el, att) {
			var res = el.attributes[att.toLowerCase()];
			if (res && res.value) {
				return res.value;
			}
		};

		var getAttributes = function (element, attributes) {
			var el = getElement(element);
			var result = {};
			if (Array.isArray(attributes)) {
				attributes.forEach(function (att) {
					var value = getAttribute(el, att);
					if (value) {
						result[att] = value;
					}
				});
			} else {
				return getAttribute(el, attributes);
			}
			return result;
		};

		var goTo = function (loc) {
			window.location = $location.protocol() + "://" + location.host + loc;
		};

		var tryOpenViewPage = function (schemaName, entityId, callback, scope) {
			openTypedPage(schemaName, entityId, "viewPage", callback, scope);
		};

		var tryOpenEditPage = function (schemaName, entityId, callback, scope) {
			openTypedPage(schemaName, entityId, "editPage", callback, scope);
		};

		var openTypedPage = function (schemaName, entityId, type, callback, scope) {
			DataService.GetEntitySchema(schemaName, function (schema) {
				var editPage = schema[type];
				if (editPage) {
					goTo("/" + editPage + "/" + entityId);
				} else {
					callback.call(scope, false);
				}
			}, scope);
		};
		return {
			getElement: getElement,
			getAttributes: getAttributes,
			goTo: goTo,
			tryOpenEditPage: tryOpenEditPage,
			tryOpenViewPage: tryOpenViewPage
		}
	})
;