angular.module("skiniaWorshipApp", ["ngCookies"])
	.factory("ModalBoxService", function ($compile, DataService) {
		var showInformationDialog = function(message, scope) {
			scope.informationDialogConfig = {
				message: message
			};
			loadModuleInModalBox("<information-dialog config='informationDialogConfig'></information-dialog>", scope);
		};

		var showConfirmationDialog = function(message, callback, scope) {
			scope.confirmationDialogConfig = {
				message: message,
				callback: callback
			};
			loadModuleInModalBox("<confirmation-dialog config='confirmationDialogConfig'></confirmation-dialog>", scope);
		};

		var showInputDialog = function(message, controls, callback, validate, scope) {
			if (Array.isArray(controls)) {
				controls.forEach(function(control) {
					if (!control.type) {
						control.type = "text"
					}
					if (!control.placeholder && control.label) {
						control.placeholder = control.label
					}
				})
			}
			scope.inputDialogConfig = {
				message: message,
				callback: callback,
				controls: controls,
				validate: validate
			};
			loadModuleInModalBox("<input-dialog config='inputDialogConfig'></input-dialog>", scope);
		};

		var showLookupDialog = function(config, callback, scope) {
			scope.lookupDialogConfig = {
				message: config.message,
				items: config.items,
				callback: callback
			};
			loadModuleInModalBox("<lookup-dialog config='lookupDialogConfig'></input-dialog>", scope);
		};

		var closeModalBox = function() {
			var elem = angular.element(document.querySelector(".modal-box"));
			elem.empty();
		};

		var loadModuleInModalBox = function (module, scope) {
			var template =
				'<div id="mask" class="no-print">' +
				'<div id="modalBox">' +
				module +
				'</div>' +
				'</div>';
			var compiled = $compile(template)(scope);
			var elem = angular.element(document.querySelector(".modal-box"));
			elem.empty();
			elem.append(compiled);
		};

		var showAddInputDialog = function(entityName, callback, scope) {
			DataService.GetEntitySchema(entityName, function (schema) {
				var columns = getDialogColumns(schema.columns, null, scope);
				showInputDialog(schema.addCaption, columns, function(code, controls) {
					if (code) {
						DataService.Insert({
							columns: getQueryColumns(columns, controls, scope),
							schemaName: schema.name
						}, function(data) {
							callback.call(scope, data);
						}, scope);
					}
				}, true, scope);
			}, scope);
		};

		var showEditInputDialog = function (item, entityName, callback, scope) {
			DataService.GetEntitySchema(entityName, function (schema) {
				var columns = getDialogColumns(schema.columns, item, scope);
				showInputDialog(schema.editCaption, columns, function(code, controls) {
					if (code) {
						DataService.Update({
							columns: getQueryColumns(columns, controls, scope),
							schemaName: schema.name,
							filters: [{columnName: "Id", value: item.Id}]
						}, function(data) {
							callback.call(scope, data);
						}, scope);
					}
				}, true, scope);
			}, scope);
		};

		var showDeleteInputDialog = function (item, entityName, callback, scope) {
			DataService.GetEntitySchema(entityName, function (schema) {
				showConfirmationDialog(schema.deleteCaption, function (code) {
					if (code) {
						DataService.Delete({
							schemaName: schema.name,
							filters: [{columnName: "Id", value: item.Id}]
						}, function (data) {
							callback.call(scope, data);
						}, scope);
					}
				}, scope);
			}, scope);
		};

		var getDialogColumns = function(schemaColumns, item, scope) {
			var columns = [];
			Object.keys(schemaColumns).map(function(key) {
				var column = schemaColumns[key];
				if (column.ForInputBox) {
					var model = item && item[column.Name] || "";
					if (column.Type === "date") {
                        model = new Date(model);
					} else if (column.Type === "checkbox") {
						if (model.data) {
                            model = model.data[0] == 1;
						} else {
                            model = model[0] == 1;
						}
					}
					var col = {
						id: column.Name,
						label: column.Caption,
						model: model,
						required: column.Required,
						type: column.Type
					};
					if (col.type === "select") {
						col.options = {};
						DataService.Select({
							columns: ["Id", "Name"],
							schemaName: column.ReferenceSchemaName
						}, function(options) {
							col.options = options;
						}, scope);
					}
					columns.push(col);
				}
			});
			return columns;
		};

		var getQueryColumns = function(columns, controls, scope) {
			var insertColumns = [];
			columns.forEach(function(column) {
				var value =  controls.find(column.id);
				if (column.type === "date") {
					value = convertDateToString(value);
				} else if (column.type === "checkbox") {
					value = value === true ? 1 : 0;
				}
				insertColumns.push({
					columnName: column.id,
					value: value
				})
			}, scope);
			return insertColumns;
		};

		var convertDateToString = function (date) {
			var month = '' + (date.getMonth() + 1),
				day = '' + date.getDate(),
				year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
			return [year, month, day].join('-');
		};

		var init = function(scope) {
			scope.schemas = {};
			scope.showInformationDialog = showInformationDialog;
			scope.showConfirmationDialog = showConfirmationDialog;
			scope.showInputDialog = showInputDialog;
			scope.showLookupDialog = showLookupDialog;
			scope.showAddInputDialog = showAddInputDialog;
			scope.showEditInputDialog = showEditInputDialog;
			scope.showDeleteInputDialog = showDeleteInputDialog;
			scope.closeModalBox = closeModalBox;
			closeModalBox(scope);
		};

		return {
			init: init
		};
	})
;