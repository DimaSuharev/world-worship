angular.module("skiniaWorshipApp", ["ngCookies"])
	.directive("informationDialog", function() {
		return {
			link: function (scope, elem, attrs) {
				var config = scope[attrs.config];
				scope.message = config.message || 'Пустое сообщение';
			},
			restrict: "E",
			templateUrl: "/modalBox/InformationDialog"
		}
	})
;