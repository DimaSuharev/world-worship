angular.module("skiniaWorshipApp", ["ngCookies"])
	.directive("lookupDialog", function() {
		return {
			link: function (scope, elem, attrs) {
				var config = scope[attrs.config];

                scope.selectedItems = [];

				scope.message = config.message || 'Пустое сообщение';
				scope.items = config.items;
				scope.resultButtonPressed = function (code) {
					if (config && typeof config.callback == "function") {
						config.callback(code, scope.selectedItems);
					}
					scope.closeModalBox()
				};

				scope.selectItem = function(item) {
					if (item.selected) {
						item.selected = "";
                        var index = scope.selectedItems.indexOf(item);
                        scope.selectedItems.splice(index, 1);
					}
					else {
                        item.selected = "background-color: #ccc;";
                        scope.selectedItems.push(item);
					}
				};

				scope.getDisabled = function () {
					return false;
				}
			},
			restrict: "E",
			templateUrl: "/modalBox/LookupDialog"
		}
	})
;