angular.module("skiniaWorshipApp", ["ngCookies"])
	.directive("confirmationDialog", function() {
		return {
			link: function (scope, elem, attrs) {
				var config = scope[attrs.config];
				scope.message = config.message || 'Пустое сообщение';
				scope.resultButtonPressed = function (code) {
					if (config && typeof config.callback == "function") {
						config.callback(code);
					}
					scope.closeModalBox()
				};
			},
			restrict: "E",
			templateUrl: "/modalBox/ConfirmationDialog"
		}
	})
;