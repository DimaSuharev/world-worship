angular.module("skiniaWorshipApp", ["ngCookies"])
	.directive("inputDialog", function() {
		return {
			link: function (scope, elem, attrs) {
				var config = scope[attrs.config];

				scope.validate = config.validate;
				scope.message = config.message || 'Пустое сообщение';

				if (config.controls) {
					scope.controls = config.controls;
					config.controls.find = function (id) {
						var model = {};
						this.forEach(function(control) {
							if (control.id === id) {
								model = control.model;
							}
						});
						return model;
					};
				}

				scope.resultButtonPressed = function (code) {
					if (config && typeof config.callback == "function") {
						config.callback(code, scope.controls);
					}
					scope.closeModalBox()
				};

				scope.getDisabled = function (formValid) {
					return scope.validate && formValid != false
				}
			},
			restrict: "E",
			templateUrl: "/modalBox/InputDialog"
		}
	})
;