angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("JsonSectionCtrl", function ($scope, $http) {
		$scope.$parent.name = "Разделы";

		$http.get("/JsonSections").then(function (response) {
			$scope.$parent["common-detail"].entitys = response.data;
			$scope.$parent["common-detail"].reload();
		});

		$scope.$parent["common-detail"] = {
			"detailName": "",
			"schemaName": "",
			"entitys": [],
			"cantEdit": true,
			"cantDelete": true,
			"cantAdd": true
		};
	})
;