angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("ObjectSectionCtrl", function ($scope, $http) {
		$scope.$parent.name = "Объекты";

		$http.get("/JsonObjects").then(function (response) {
			$scope.$parent["common-detail"].entitys = response.data;
			$scope.$parent["common-detail"].reload();
		});

		$scope.$parent["common-detail"] = {
			"detailName": "",
			"schemaName": "",
			"entitys": [],
			"cantEdit": true,
			"cantDelete": true
		};
	})
;