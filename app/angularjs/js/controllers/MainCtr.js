angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("MainCtr", function ($scope, $http, $window, $location, AttributeService, ModalBoxService, $rootScope, $compile, $cookies) {

		//region init: GoogleAnalytics

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-74372721-1', 'auto');
		ga('send', 'pageview');

		//endregion

		$scope.nemuClick = function () {
			$scope.showMenu = !$scope.showMenu;
		};

		$scope.mainMemuItemClick = function(item) {
			if (typeof $scope[item] === "function") {
				$scope[item]();
			}
		};

		$scope.onScroll = function() {
			var menu = document.querySelector(".header-menu");
			window.scrollY >= 98
				? menu.classList.add('fixed')
				: menu.classList.remove('fixed');
			var navigationMenu = document.querySelector("navigation-menu");
			window.scrollY >= 98
				? navigationMenu.classList.add('fixed-nemu')
				: navigationMenu.classList.remove('fixed-nemu');
		};

		document.addEventListener("scroll", $scope.onScroll);
		//---------------------- Потом вырезать----------------------
		$scope.nemuItemClick = function(section) {
			if (section) {
				AttributeService.goTo("/#" +section);
			}
			$scope.nemuClick();
		};
		var changeMode = function(arg1, arg2, oldPath) {
			$scope.showMenu = false;
			var path = $location.path();
			var pathArray = path.split("/");
			var appState = {
				section: pathArray[1],
				mode: pathArray[2] || pathArray[1],
				entityId: pathArray[3]
			};
			if (oldPath && oldPath !== appState.section) {
				$scope.loadModulesByPath();
			}
			$scope.$parent.appState = appState;
			if (typeof $scope.$parent.init === "function")
				$scope.$parent.init(appState);
			$window.scrollTo(0, 0);
		};

		$rootScope.$on('$locationChangeSuccess', changeMode);

		$scope.loadMainModule = function (module, callback, scope1) {
			var elem = angular.element(document.querySelector(".content-box"));
			var compiled = $compile(module)($scope);
			elem.empty();
			elem.append(compiled);
			if (typeof callback === "function") {
				callback.call(scope1 || $scope);
			} else if (typeof callback === "object") {
				changeMode.call(scope1);
			}
		};

		$scope.loadModulesByPath = function() {

			$scope.showMenu = false;
			var path = $location.path();
			var pathArray = path.split("/");
			var moduleName = "";
			switch (pathArray[1]) {
				case "CardModule":
					moduleName = "<card-module></card-module>";
					break;
				case "MainSection":
					moduleName = "<main-page></main-page>";
					break;
				case "SchedulerSection":
					moduleName = "<scheduler-page></scheduler-page>";
					break;
				case "IventSection":
					moduleName = "<ivents-main></ivents-main>";
					break;
				case "CategorySection":
					moduleName = "<song-menu></song-menu>";
					break;
				case "ChatSection":
					moduleName = "<chat></chat>";
					break;
				case "SystemSection":
					moduleName = "<system-section></system-section>";
					break;
			}
			if (moduleName === "") {
				return;
			}
			$scope.loadMainModule(moduleName, pathArray);
		};
		//--------------------------------------------------------

		$scope.backClick = function () {
			$window.history.back();
		};

		var att = AttributeService.getAttributes(".content-box", [
			"recordid",
			"entityname"
		]);

		if (att) {
			$scope.entityName = att.entityname;
			$scope.entityId = att.recordid;
		}

		$scope.loginUser = function(notLogin) {
			var session = $cookies.get("session");
			if (session) {
                if ($scope.currentUser &&!notLogin) {
                    $scope.showConfirmationDialog("Выйти?", function (code) {
                        if (code) {
                            $scope.currentUser = {};
                            $cookies.put("session", "");
                        }
                    }, $scope);
                }
				session = JSON.parse(atob(session));
				if (session.Id) {
					$scope.currentUser = session;
				}
			}
			if (!notLogin && !session.Id) {
				$scope.showInputDialog("Войти", [{
					id: "Login",
					label: "Login"
				}, {
					id: "Password",
					label: "Password",
					type: "password"
				}], function(code, controls) {
					if (code) {
						var login = controls.find("Login");
						var password = controls.find("Password");
						if (!login || !password) {
							return;
						}
						$http.post("/Login", {
							userId: login,
							userPass: password
						}).then(function (response) {
							if (!response.data) {
								$scope.showInformationDialog("Не верный пароль.");
							}
							$scope.currentUser = response.data;
						}, $scope.errorHandler);
					}
				}, true, $scope);
			}

		};

		$scope.errorHandler = function (response) {
			$scope.showInformationDialog(response.statusText);
		};

		$scope.loginUser(true);

		ModalBoxService.init($scope);
	})
;