angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("TileDetailCtr", function ($scope, $attrs, AttributeService, DataService) {
		var config = $scope[$attrs.config];
        $scope.config = config;
		$scope.detailName = config.detailName;
		$scope.cantEdit = config.cantEdit;
		$scope.cantDelete = config.cantDelete;
		$scope.cantAdd = config.cantAdd;
        $scope.showFilter = config.showFilter;

		var getEntitys = function () {
			if(Array.isArray(config.entitys)) {
				$scope.entitys = config.entitys;
				return;
			}
			var conf = {
				columns: config.columns || ["*"],
				schemaName: config.schemaName
			};
			if (config.filters) {
                config.filters.forEach(function (filter) {
                	if (filter["fn-value"] && !filter.value) {
                        filter.value = eval(filter["fn-value"]);
					}
				}, $scope);
				conf.filters = config.filters;
			}
			if (config.order) {
				conf.order = config.order;
			}
			DataService.Select(conf, function (entitys) {
				if (typeof config.prepareEntitys === "function") {
					config.prepareEntitys.call(config.scope || this, entitys, function() {
						$scope.entitys = entitys;
					});
				} else {
					$scope.entitys = entitys;
					if (typeof config.callback === "function") {
						config.callback.call(config.scope, entitys);
					}
				}
			}, $scope);
		};

        $scope.getEntitys = getEntitys;

		getEntitys();

		$scope.addEntity = function () {
			if (typeof config.addFunction === "function") {
				config.addFunction.call($scope, function() {
					getEntitys();
				}, $scope);
			} else {
				$scope.showAddInputDialog(config.schemaName, function() {
					getEntitys();
				}, $scope);
			}
		};
		$scope.deleteEntity = function (entity, event) {
			event.stopPropagation();
			if (typeof config.deleteFunction === "function") {
				config.addFunction.call($scope, entity, function() {
					getEntitys();
				}, $scope);
			} else {
				$scope.showDeleteInputDialog(entity, config.schemaName, function () {
					getEntitys();
				}, $scope);
			}
		};
		$scope.editEntity = function (entity, event) {
			event.stopPropagation();
			if (typeof config.editFunction === "function") {
				config.addFunction.call($scope, entity, function() {
					getEntitys();
				}, $scope);
			} else {
				AttributeService.tryOpenEditPage(config.schemaName, entity.Id, function (succsess) {
					if (!succsess) {
						$scope.showEditInputDialog(entity, config.schemaName, function () {
							getEntitys();
						}, $scope);
					}
				}, $scope);
			}
		};
		$scope.selectEntity = function (entity) {
			if(entity.GoTo) {
				AttributeService.goTo(entity.GoTo);
			} else if (typeof config.selectFunction === "function") {
				config.selectFunction.call($scope, entity);
			} else {
				AttributeService.tryOpenViewPage(config.schemaName, entity.Id, function () {}, $scope);
			}
		};

		$scope[$attrs.config].reload = getEntitys.bind($scope);
	})
;