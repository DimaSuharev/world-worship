angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("DetailCtrl", function ($scope, $attrs, $compile, AttributeService, DataService) {
		var config = $scope[$attrs.config];
        $scope.config = config;
		$scope.detailName = config.detailName;
		$scope.cantEdit = config.cantEdit;
		$scope.cantDelete = config.cantDelete;
		$scope.cantAdd = config.cantAdd;
        $scope.showFilter = config.showFilter;

		var template =
			"<li class=\"list-group-item\" ng-repeat=\"entity in entitys | orderBy: 'Position'\" ng-click=\"selectEntity(entity)\">" +
			"<div class=\"btn svg-btn edit-button\" ng-click=\"editEntity(entity, $event)\" ng-if=\"currentUser.CanEdit && !cantEdit\">C</div>" +
			"<div class=\"btn svg-btn delete-button\" ng-click=\"deleteEntity(entity, $event)\" ng-if=\"currentUser.CanEdit&& !cantDelete\">D</div>" +
			"#gridConfig#" +
			"</li>";
		var defaultGridConfig = "<img ng-src=\"{{entity.ImageUrl}}\" alt='img'/><a> {{entity.Name}}</a>";

		var init = function () {
			var queryResult = AttributeService.getElement(".detail[config=\"" + $attrs.config + "\"] .grid");
			var elem = angular.element(queryResult);

			template = template.replace("#gridConfig#", config.gridConfig || defaultGridConfig);
			var compiled = $compile(template)($scope);
			elem.empty();
			elem.append(compiled);
		};

		init();

		var initEvents = false;

		var getEntitys = function () {
			if(Array.isArray(config.entitys)) {
				$scope.entitys = config.entitys;
				return;
			}
			var conf = {
				columns: config.columns || ["*"],
				schemaName: config.schemaName
			};
			if (config.filters) {
                config.filters.forEach(function (filter) {
                    if (filter["fn-value"] && !filter.value) {
                        filter.value = eval(filter["fn-value"]);
                    }
                }, $scope);
				conf.filters = config.filters;
			}
			if (config.order) {
				conf.order = config.order;
			}
			DataService.Select(conf, function (entitys) {
				if (!initEvents) {
                    var queryResult = AttributeService.getElement("#filter input", true);
                    queryResult.forEach(function (res) {
                        var elem = angular.element(res);
                        elem.on('keypress', function (e) {
                                if (e.keyCode == 13) {
                                    getEntitys();
                                    return false;
                                }
                            }
                        );
                    });
                    initEvents = true;
				}

				if (typeof config.prepareEntitys === "function") {
					config.prepareEntitys.call(config.scope || this, entitys, function() {
						$scope.entitys = entitys;
					});
				} else {
					$scope.entitys = entitys;
					if (typeof config.callback === "function") {
						config.callback.call(config.scope, entitys);
					}
				}
			}, $scope);
		};

		$scope.getEntitys = getEntitys;

		$scope.addEntity = function () {
			if (typeof config.addFunction === "function") {
				config.addFunction.call($scope, function() {
					getEntitys();
				}, $scope);
			} else {
				$scope.showAddInputDialog(config.schemaName, function() {
					getEntitys();
				}, $scope);
			}
		};
		$scope.deleteEntity = function (entity, event) {
			event.stopPropagation();
			if (typeof config.deleteFunction === "function") {
				config.deleteFunction.call($scope, entity, function() {
					getEntitys();
				}, $scope);
			} else {
				$scope.showDeleteInputDialog(entity, config.schemaName, function () {
					getEntitys();
				}, $scope);
			}
		};
		$scope.editEntity = function (entity, event) {
			event.stopPropagation();
			if (typeof config.editFunction === "function") {
				config.editFunction.call($scope, entity, function() {
					getEntitys();
				}, $scope);
			} else {
				AttributeService.tryOpenEditPage(config.schemaName, entity.Id, function (succsess) {
					if (!succsess) {
						$scope.showEditInputDialog(entity, config.schemaName, function () {
							getEntitys();
						}, $scope);
					}
				}, $scope);
			}
		};
		$scope.selectEntity = function (entity) {
			if (typeof config.selectFunction === "function") {
				config.selectFunction.call($scope, entity);
			} else {
				AttributeService.tryOpenViewPage(config.schemaName, entity.Id, function () {}, $scope);
			}
		};

		getEntitys();

		$scope[$attrs.config].reload = getEntitys.bind($scope);
	})
;