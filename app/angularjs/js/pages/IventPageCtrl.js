angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("IventPageCtrl", function ($scope, $http, $sce, $compile, AttributeService, DataService) {
		$scope.$parent.name = AttributeService.getAttributes(".ivent-page", "recordName");

		$scope.$parent.mapInit = false;

		window.initMap = function() {
			$http.get('https://maps.googleapis.com/maps/api/geocode/json?address=Киев,' + window.address + '&key=AIzaSyCjn0hOcN786uvjNsv0Hw_Kkg8yR29Qt2k')
				.success( function(response, succsess) {
						var results = response.results;
						if (succsess == 200) {
							var myOptions = {
								zoom: 17,
								center: results[0].geometry.location,
								scrollwheel: false,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							};
							$scope.maps = new google.maps.Map(AttributeService.getElement("#map"), myOptions);
							$scope.marker = new google.maps.Marker({ // маркер
								map: $scope.maps,
								visible: true,
								title: address,
								position: results[0].geometry.location
							});

							var infowindow = new google.maps.InfoWindow({
								content: '<div style="color: black;">' + address + '</div>'
							});
							$scope.marker.addListener('click', function() {
								infowindow.open($scope.maps, $scope.marker);
							});
						} else {
							alert('Geocode was not successful for the following reason: ' + status);
						}
					}
				);
		};

		$scope.$parent.renderMap = function(address) {
			window.address = address;
			if (!$scope.$parent.mapInit) {
				var script = document.createElement("script");
				script.setAttribute("src", "https://maps.googleapis.com/maps/api/js?key=AIzaSyCjn0hOcN786uvjNsv0Hw_Kkg8yR29Qt2k&callback=initMap");
				document.head.append(script);
			}
		};
		var youtubeTemplate = '<div class="no-print youtube-video"  style="text-align: center" ng-if="showVideo">' +
			'<iframe width="100%" height="100%" src="{{iventPlayList}}" frameborder="0" allowfullscreen/>' +
			'</div>';

		$scope.$parent.youtubePlaylistUrl = "";
		$scope.$parent.loadYouTube = function () {
			if ($scope.$parent.youtubePlaylistUrl !== "") {
				$scope.iventPlayList = $sce.trustAsResourceUrl($scope.$parent.youtubePlaylistUrl);
				var queryResult = AttributeService.getElement('#youtube-video');
				var wrappedQueryResult = angular.element(queryResult);
				var compiled = $compile(youtubeTemplate)($scope);
				wrappedQueryResult.empty();
				wrappedQueryResult.append(compiled);
			}
		};
		$scope.$parent["song-detail"] = {
			gridConfig: "<a>{{entity.Position}}. {{entity.Name}}</a>",
			detailName: "Песни",
			schemaName: "SongInIvent",
			columns: ["*", "SongId.Name as Name", "SongId.YouTubeUri as YouTubeUri"],
			filters: [{columnName: "IventId", "cond": "=",  value: $scope.entityId}],
			addFunction: function (callback, scope) {
				DataService.Select({
					columns: ["Id", "Name"],
					schemaName: "Song",
					filters: [{
						columnName: "SongId",
						cond: "not exists",
						value: "(select 1 from SongInIvent where SongId = Song.Id and IventId = '" + $scope.entityId + "')"
					}]
				}, function (songs) {
					$scope.showLookupDialog({
						message: "Песни",
						items: songs
					}, function (code, songs) {
						if (code) {
                            songs.forEach(function (song) {
                                DataService.Insert({
                                    columns: [
                                        {columnName: "SongId", value: song.Id},
                                        {columnName: "IventId", value: $scope.entityId},
                                        {columnName: "Position", value: 0}
                                    ],
                                    schemaName: "SongInIvent"
                                }, function () {
                                    callback.call(scope);
                                }, $scope);
							}, $scope);
						}
					}, $scope);
				}, $scope);
			},
			selectFunction: function (entity) {
				AttributeService.tryOpenViewPage("Song", entity.SongId, function () {}, $scope);
			},
			callback: function (entitys) {
				var youtubePlaylistUrl = "http://www.youtube.com/embed/?autoplay=1&playlist=";
				var songsYoutubeCodes = [];
				entitys.forEach(function (song) {
					if (song["SongId.YouTubeUri"] != "") {
						var urlArray = song["YouTubeUri"].split("v=");
						songsYoutubeCodes.push(urlArray[urlArray.length - 1]);
					}
				}, $scope);
				$scope.showVideo = songsYoutubeCodes.length > 0;
				if (songsYoutubeCodes.length > 0) {
					$scope.$parent.youtubePlaylistUrl = youtubePlaylistUrl + songsYoutubeCodes.join(",");
				}
			},
			scope: $scope
		};

		$scope.$parent["user-detail"] = {
		    gridConfig: "<a> {{entity.Name}}</a><img ng-repeat='image in entity.roles' style='background-color: rgba(200, 200, 200, 01);margin-left: 15px;border-radius: 5px;' ng-src=\"{{image}}\" alt='img'/>",
			detailName: "Участники",
			schemaName: "VwUserInIvent",
			columns: ["*", "UserId.Name as Name", "ImageUrl"],
			filters: [{columnName: "IventId", "cond": "=",  value: $scope.entityId}],
            order: ["Name"],
			addFunction: function (callback, scope) {
				DataService.Select({
					columns: ["*"],
					schemaName: "User",
					filters: [{
						cond: "not exists",
						value: "(select 1 from UserInIvent where UserId = User.Id and IventId = '" + $scope.entityId + "')"
					}, {
                        "columnName": "Active",
						"value": "1"
					}]
				}, function (users) {
					$scope.showLookupDialog({
						message: "Участники",
						items: users
					}, function (code, users) {
						if (code) {
							users.forEach(function (user) {
                                DataService.Insert({
                                    columns: [
                                        {columnName: "UserId", value: user.Id},
                                        {columnName: "IventId", value: $scope.entityId}
                                    ],
                                    schemaName: "UserInIvent"
                                }, function () {
                                    DataService.Insert({
                                        columns: [
                                            {columnName: "UserId", value: user.Id},
                                            {columnName: "IventId", value: $scope.entityId},
                                            {columnName: "RoleId", value: user.DefaultRoleId}
                                        ],
                                        schemaName: "UserInIventRole"
                                    }, function () {
                                        callback.call(scope);
                                    }, $scope);
                                }, $scope);
							}, $scope);
						}
					}, $scope);
				}, $scope);
			},
			editFunction: function (entity, callback, scope) {
                DataService.Select({
                    columns: ["*"],
                    schemaName: "UserRole"
                }, function (roles) {
                    $scope.showLookupDialog({
                        message: "Роли для " + entity.Name,
                        items: roles
                    }, function (code, roles) {
                        if (code) {
                        	DataService.Delete({
                                schemaName: "UserInIventRole",
                                filters: [{
                                    columnName: "IventId",
                                    value: $scope.entityId
                                }, {
                                    columnName: "UserId",
                                    value: entity.UserId
                                }]
							}, function () {
                                roles.forEach(function (role) {
                                    DataService.Insert({
                                        columns: [
                                            {columnName: "UserId", value: entity.UserId},
                                            {columnName: "IventId", value: $scope.entityId},
                                            {columnName: "RoleId", value: role.Id}
                                        ],
                                        schemaName: "UserInIventRole"
                                    }, function () {
                                        callback.call(scope);
                                    }, $scope);
                                }, $scope);
                            }, $scope);

                        }
                    }, $scope);
                }, $scope);
            },
            prepareEntitys: function (entitys, callback) {
                if(!Array.isArray(entitys)) {
                    return;
                }
                entitys.forEach(function (item) {
                    item.roles = item.ImageUrl.split(",");
                });
                callback();
            },
			deleteFunction: function (item, callback, scope) {
				$scope.showDeleteInputDialog(item, "UserInIvent", function() {
                    callback.call(scope);
				}, $scope);
            }
		};
	})
;