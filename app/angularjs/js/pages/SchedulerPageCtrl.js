angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("SchedulerPageCtrl", function ($scope, DataService) {
		$scope.$parent.name = "Расписание";

		$scope.$parent.addHours = function(entity, h) {
			var d = new Date(entity.Date);
			d.setTime(d.getTime() + (h*60*60*1000));
			return convertDateToString(d);
		};

		var convertDateToString = function(date) {
			var month = '' + (date.getMonth() + 1),
				day = '' + date.getDate(),
				year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;
			return [year, month, day].join('-');
		};

		var getUsers = function(iventId, callback, scope) {
			DataService.Select({
				columns: ["Id", "UserId.Name as Name", "ImageUrl", "UserId"],
				schemaName: "VwUserInIvent",
                filters: [{columnName: "IventId", "cond": "=",  value: iventId}]
			}, function(users) {
				callback.call(scope, users);
			}, $scope);
		};

		$scope.$parent["ivent-detail"] = {
			detailName: "",
            gridConfig: '<a> {{entity.Name}}</a>' +
			'<div class="sh-ivent" ng-repeat="ivent in entity.ivents">' +
			'<span class="sh-empty">{{ivent.Date}}</span>' +
			'<img ng-repeat=\'image in ivent.roles\' style=\'background-color: rgba(200, 200, 200, 01);margin: 0 5px;border-radius: 5px;\' ng-if="image" ng-src="{{image}}" style="width: 26px">' +
			'</div>',
			schemaName: "User",
			filters: [{columnName: "Active", cond: "=", value: 1}, {"columnName": "Name", "cond": "like", "value": ""}],
            order: ["Name"],
            prepareEntitys: function(shUsers, callback) {
                DataService.Select({
                    columns: ["*"],
                    schemaName: "Event",
                    filters: [{columnName: "Date", "cond": ">=",  value: convertDateToString(new Date())}],
                    order: ["Date"]
                }, function(ivents) {
                    var start = 0;
					ivents.forEach(function (ivent) {
                        ivent.Date = convertDateToString(new Date(ivent.Date));
						getUsers(ivent.Id, function(users) {
							start++;
							ivent.users = users;
							ivent.users.forEach(function (item) {
							   item.roles = item.ImageUrl.split(",");
							});
							if (start === ivents.length) {
								shUsers.forEach(function (role) {
									var iv = [];
									ivents.forEach(function (ivent) {
										var isAdd = false;
										ivent.users.forEach(function (us) {
											if (us.UserId === role.Id) {
												iv.push({roles: us.roles});
												isAdd = true;
											}
										});
										if (!isAdd) {
											iv.push({roles: []});
										}
									});
									role.ivents = iv;
								});
								shUsers.unshift({
									Name: "",
									ivents: ivents
								});
								callback();
							}
						}, $scope);
					});
                }, $scope);

            },
			cantEdit: true,
			cantDelete: true,
			cantAdd: true
		};
	})
;
