angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("SongPageCtrl", function ($scope, $sce, AttributeService) {
		$scope.$parent.name = AttributeService.getAttributes(".song-page", "recordName");

		var url = AttributeService.getAttributes(".song-page", "youtube");
		if (url) {
            var youtubeUrl = url.split("v=");
            $scope.$parent.YouTubeUri = (youtubeUrl)
                ? $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + youtubeUrl[youtubeUrl.length - 1])
                : null;
		}

		$scope.print = function () {
			setTimeout(window.print, 1000);
		};

		$scope.showBass = false;

		$scope.changeChordView = function() {
			$scope.showBass = !$scope.showBass;
		};

		$scope.textSize = window.localStorage.getItem("textSize") || 15;

		$scope.plusTextSize = function() {
			$scope.textSize++;
			window.localStorage.setItem("textSize", $scope.textSize);
		};

		$scope.minusTextSize = function() {
			$scope.textSize--;
			window.localStorage.setItem("textSize", $scope.textSize);
		};

		$scope.setEditMode = function (entityId) {
			AttributeService.tryOpenEditPage("Song", entityId, function(){}, $scope);
		};

		$scope.$parent["song-detail"] = {
			gridConfig:
			"<h4 style='font-style: italic; text-align: center;font-weight: bold;font-size: {{textSize}}px;margin-top: 0;'>{{entity.Name}}</h4>" +
			"<div ng-repeat='row in entity.rows'>" +
			"<div class='song-chords'>" +
			"<p ng-if='!showBass && row.chords' style='font-size: {{textSize}}px;'>{{row.chords}}<br></p>" +
			"<p ng-if='showBass && row.bassChords' style='font-size: {{textSize}}px;'>{{row.bassChords}}<br></p>" +
			"</div>" +
			"<div class='song-text'>" +
			"<p style='font-size: {{textSize}}px'>{{row.text}}<br></p>" +
			"</div>" +
			"</div>",
			detailName: "",
			schemaName: "SongPart",
			filters: [{columnName: "SongId", value: $scope.entityId}],
			prepareEntitys: function (entitys, callback) {
				if(!Array.isArray(entitys)) {
					return;
				}
				entitys.forEach(function (item) {
					var rows = {};
					var chords = item.Chords;
					var bassChords = item.BassChords;
					var text = item.Text;
					var number = 100;
					var chordsArray = chords.split("\n");
					var bassChordsArray = bassChords.split("\n");
					var textArray = text.split("\n");
					while (chordsArray.length !== 0 || textArray.length !== 0) {
						var ch = "", tx = "", bch = "";
						if (bassChordsArray.length !== 0) {
							bch = bassChordsArray.shift();
						}
						if (chordsArray.length !== 0) {
							ch = chordsArray.shift();
						}
						if (textArray.length !== 0) {
							tx = textArray.shift();
						}
						rows[number++] = {
							chords: ch,
							bassChords: bch,
							text: tx
						};
					}
					item.rows = rows;
				});
				callback();
			},
			cantEdit: true,
			cantDelete: true,
			cantAdd: true
		};
	})
;