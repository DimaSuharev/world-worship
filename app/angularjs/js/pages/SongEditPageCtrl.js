angular.module("skiniaWorshipApp", ["ckeditor", "ngCookies"])
	.controller("SongEditPageCtrl", function ($scope, $http, AttributeService, DataService) {

		var editor;
		ClassicEditor
            .create(document.querySelector('#textarea-body'), {
                toolbar: ['bold', 'italic', '|', 'undo', 'redo']
			}).then(function(newEditor) {
            	editor = newEditor;
    		} );

        $scope.$parent.name = "Редактор песен";

		$http.get("/SongId=" + $scope.entityId).success(function (response) {
			$scope.song = response;
            editor.setData($scope.song.Body);
		});

		$scope.addSongPart = function(parts) {
			parts.push({
				name: "",
				text: "",
				chords: ""
			});
		};
		$scope.saveSong = function() {
			var song = $scope.song;
            song.Body = editor.getData();
			$http.post("/SongId=" + $scope.entityId, $scope.song).then(function () {
				$scope.showInformationDialog("Успешно сохранено", $scope)
			}, $scope.errorHandler);
		};


		$scope.deleteSongPart = function(item) {
			if (item.id) {
				DataService.Delete({
					schemaName: "SongPart",
					filters: [
						{columnName: "Id", value: item.id}
					]
				}, function () {
					var index = $scope.song.parts.indexOf(item);
					$scope.song.parts.splice(index, 1);
				}, $scope);
			} else {
				var index = $scope.song.parts.indexOf(item);
				$scope.song.parts.splice(index, 1);
			}
		};

		$scope.convert = function (item) {
			$scope.showInputDialog("Распарсить", [{
				id: "text",
				label: "Текст",
				type: "textarea",
				rows: 10
			}], function(code, controls) {
				if (code) {
					var text = controls.find("text");
					if (!text) {
						return;
					}
					var parts = text.split("\n");
					if (parts[1] === "") {
						item.name = parts.shift();
						parts.shift();
					}
					var chords = [];
					var texts = [];
					while(parts.length > 0) {
						if (parts.length > 0) {
							chords.push(parts.shift());
						}
						if (parts.length > 0) {
							texts.push(parts.shift());
						}
					}
					item.chords = chords.join("\n");
					item.text = texts.join("\n");
				}
			}, true, $scope);
		}
	})
;