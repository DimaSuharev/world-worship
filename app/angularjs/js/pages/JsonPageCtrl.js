angular.module("skiniaWorshipApp", ["ngCookies"])
	.controller("JsonPageCtrl", function ($scope, $http, AttributeService) {
		var reg = /[A-Z][a-z]*/g;
		$scope.$parent.name = AttributeService.getAttributes(".content-box", "entityName").match(reg).join(" ");

		var recordId = AttributeService.getAttributes(".content-box", "recordId");

		$http.get("/json/sections/" + recordId).success(function (response) {
			$scope.$parent.sectionName = response.sectionName;
			$scope.$parent.mode = response.mode;
			$scope.$parent["entitys-detail"].entitys = response.detail.entitys;
			if(Array.isArray(response.detail.columns)) {
				response.detail.columns.forEach(function (item) {
					$scope.$parent["colums-detail"].entitys.push({
						Name: item
					})
				});
			}
			if(Array.isArray(response.detail.filters)) {
				response.detail.filters.forEach(function (item) {
					$scope.$parent["filters-detail"].entitys.push(item)
				});
			}
			$scope.$parent.detail = response.detail;
		}, $scope.errorHandler);

		$scope.$parent.saveJson = function () {
			var config = {
				sectionName: $scope.sectionName,
				mode: $scope.mode,
				detail: $scope.detail
			};
			$scope.detail.columns = [];
			$scope.$parent["colums-detail"].entitys.forEach(function (column) {
				$scope.detail.columns.push(column.Name);
			});
			$scope.detail.filters = [];
			$scope.$parent["filters-detail"].entitys.forEach(function (filter) {
				$scope.detail.filters.push(filter);
			});
			$http.post("/SaveJson/" + recordId, config).success(function (res) {
				$scope.showInformationDialog(res, $scope);
			}, $scope.errorHandler);
		};

		$scope.$parent["colums-detail"] = {
			gridConfig: "<div><span style='color: #5cb85c'>Колонка: </span>[{{entity.Name}}]</div>",
			detailName: "Колонки",
			entitys: [],
			addFunction: function (callback, scope) {
				$scope.showInputDialog("Добавить колонку", [{
					id: "Name",
					label: "Name"
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						if (!Name) {
							return;
						}
						if ($scope.detail.columns) {
							$scope.$parent["colums-detail"].entitys.push({
								Name: Name
							});
						} else {
							$scope.$parent["colums-detail"].entitys = [
								{
									Name: Name
								}
							];
						}
						callback.call(scope);
					}
				}, true, $scope);
			},
			editFunction: function (entity, callback, scope) {
				$scope.showInputDialog("Изменить колонку", [{
					id: "Name",
					label: "Name",
					model: entity.Name
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						var index = $scope.$parent["colums-detail"].entitys.indexOf(entity);
						$scope.$parent["colums-detail"].entitys[index] = {
							Name: Name
						};
						callback.call(scope);
					}
				}, true, $scope);
			},
			deleteFunction: function (entity, callback, scope) {
				$scope.showConfirmationDialog("Удалить колонку?", function (code) {
					if (code) {
						var index = $scope.$parent["colums-detail"].entitys.indexOf(entity);
						$scope.$parent["colums-detail"].entitys.splice(index, 1);
						callback.call(scope);
					}
				}, $scope);
			}
		};

		$scope.$parent["filters-detail"] = {
			gridConfig: "<div><span style='color: #5cb85c'>Фильтр по: </span>[{{entity.columnName}}]</div>",
			detailName: "Фильтр",
			entitys: [],
			addFunction: function (callback, scope) {
				$scope.showInputDialog("Добавить фильтр", [{
					id: "Name",
					label: "Name"
				}, {
					id: "cond",
					label: "Условие"
				}, {
					id: "value",
					label: "Значение"
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						var cond = controls.find("cond");
						var value = controls.find("value");
						if (!Name) {
							return;
						}
						if (scope.$parent["filters-detail"].entitys) {
							$scope.$parent["filters-detail"].entitys.push({
								columnName: Name,
								cond: cond,
								value: value
							});
						} else {
							$scope.$parent["filters-detail"].entitys = [
								{
									columnName: Name,
									cond: cond,
									value: value
								}
							];
						}
						callback.call(scope);
					}
				}, true, $scope);
			},
			editFunction: function (entity, callback, scope) {
				$scope.showInputDialog("Изменить фильтр", [{
					id: "Name",
					label: "Колонка",
					model: entity.columnName
				}, {
					id: "cond",
					label: "Условие",
					model: entity.cond
				}, {
					id: "value",
					label: "Значение",
					model: entity.value
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						var cond = controls.find("cond");
						var value = controls.find("value");
						var index = $scope.$parent["filters-detail"].entitys.indexOf(entity);
						$scope.$parent["filters-detail"].entitys[index] = {
							columnName: Name,
							cond: cond,
							value: value
						};
						callback.call(scope);
					}
				}, true, $scope);
			},
			deleteFunction: function (entity, callback, scope) {
				$scope.showConfirmationDialog("Удалить фильтр?", function (code) {
					if (code) {
						var index = $scope.$parent["filters-detail"].entitys.indexOf(entity);
						$scope.$parent["filters-detail"].entitys.splice(index, 1);
						callback.call(scope);
					}
				}, $scope);
			}
		};

		$scope.$parent["entitys-detail"] = {
			gridConfig: "<div><span style='color: #5cb85c'>Заголовок: </span>{{entity.Name}}</div>",
			detailName: "Елементы раздела",
			entitys: [],
			addFunction: function () {
				$scope.showInputDialog("Добавить елемент детали", [{
					id: "Name",
					label: "Name"
				}, {
					id: "ImageUrl",
					label: "ImageUrl"
				}, {
					id: "GoTo",
					label: "GoTo"
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						var ImageUrl = controls.find("ImageUrl");
						var GoTo = controls.find("GoTo");
						if (!Name || !ImageUrl || !GoTo) {
							return;
						}
						$scope.$parent.detail.entitys.push({
							Name: Name,
							ImageUrl: ImageUrl,
							GoTo: GoTo
						})
					}
				}, true, $scope);
			},
			editFunction: function (entity, callback, scope) {
				$scope.showInputDialog("Изменить елемент детали", [{
					id: "Name",
					label: "Name",
					model: entity.Name
				}, {
					id: "ImageUrl",
					label: "ImageUrl",
					model: entity.ImageUrl
				}, {
					id: "GoTo",
					label: "GoTo",
					model: entity.GoTo
				}], function(code, controls) {
					if (code) {
						var Name = controls.find("Name");
						var ImageUrl = controls.find("ImageUrl");
						var GoTo = controls.find("GoTo");
						entity.Name = Name;
						entity.ImageUrl = ImageUrl;
						entity.GoTo = GoTo;
						callback.call(scope);
					}
				}, true, $scope);
			},
			deleteFunction: function (entity, callback, scope) {
				$scope.showConfirmationDialog("Удалить елемент?", function (code) {
					if (code) {
						var index = $scope.$parent.detail.entitys.indexOf(entity);
						$scope.$parent.detail.entitys.splice(index, 1);
						callback.call(scope);
					}
				}, $scope);
			}
		};

		$scope.$parent.currentUser = {CanEdit: true}
	})
;