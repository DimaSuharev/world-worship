var mysql  = require('./mysql-model.js');
var config = require("../../app/config");

module.exports = function (app) {
    var mysqlModel = mysql.createConnection({
        host     : config.mysqlHost,
        port     : config.mysqlPort,
        user     : config.mysqlUser,
        password : config.mysqlPass,
        database : 'worldwor_songs'
    });
    app.connection = mysqlModel;
    app.esq = require("./esq").init(mysqlModel);
};
