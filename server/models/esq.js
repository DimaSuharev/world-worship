var entitySchemas = require("./Documents").documents();

exports.init = function(MyAppModel) {
    var sqlModel = new MyAppModel();

    var parceWhereSection = function(filters) {
        var where = "where ";
        if (typeof filters === "object") {
            filters.forEach(function(condition, number) {
                var cond = condition.cond || "=";
                if (number === 0) {
                    if (cond === "is null" || cond === "is not null") {
                        where += "{0} {1} ".format(condition.columnName, cond);
                    } else if (cond === "not exists" || cond === "exists") {
                        where += "{0} {1} ".format(cond, condition.value);
                    } else if (cond === "not like" || cond === "like") {
                        where += "{0} {1} '%{2}%' ".format(condition.columnName, cond, condition.value);
                    } else {
                        where += "{0} {1} '{2}' ".format(condition.columnName, cond, condition.value);
                    }
                } else {
                    if (cond === "is null" || cond === "is not null") {
                        where += "and {0} {1} ".format(condition.columnName, cond);
                    } else if (cond === "not exists" || cond === "exists") {
                        where += "and {0} {1} ".format(cond, condition.value);
                    } else if (cond === "not like" || cond === "like") {
                        where += "and {0} {1} '%{2}%' ".format(condition.columnName, cond, condition.value);
                    } else {
                        where += "and {0} {1} '{2}' ".format(condition.columnName, cond, condition.value);
                    }
                }
            }, this);
        } else {
            return;
        }
        return where;
    };

    var getReferenceSchemaName = function(schemaName, column) {
        var schema = entitySchemas[schemaName];
        var col = schema.columns[column];
        return col.ReferenceSchemaName;
    };

    var parceSelectConfig = function(columns, schemaName, filters, orderby) {
        var select = "select ";
        var from = "from {0} ".format(schemaName);
        var where = parceWhereSection(filters);
        var order = "order by {0} ";
		if (!orderby) {
			orderby = []
		}
        var join = {};
        columns.forEach(function(column, number) {
            var columnArray = column.split(".");
            if (columnArray.length > 1) {
                if (columnArray[columnArray.length - 1].indexOf(" as ")) {
                    var parts = columnArray[columnArray.length -1].split(" as ");
                    columnArray[columnArray.length -1] = parts[0];
                    var asColumn = parts[1];
                }
                var nextReferenceSchemaName = "";
                for (var i = 0; i < columnArray.length - 1; i++) {
                    var referenceSchemaName = getReferenceSchemaName(nextReferenceSchemaName || schemaName, columnArray[i]);
                    column = "{0}.{1} as '{2}'".format(referenceSchemaName, columnArray[i + 1], asColumn || columnArray.join("."));
                    if (!join["referenceSchemaName" + referenceSchemaName]) {
                        from += " left join {0} on {1}.{2} = {3}.Id".format(
                            referenceSchemaName,
                            nextReferenceSchemaName || schemaName,
                            columnArray[i],
                            referenceSchemaName
                        );
                    }
                    nextReferenceSchemaName = referenceSchemaName;
                    join["referenceSchemaName" + referenceSchemaName] = true;
                }
            } else {
                column = "{0}.{1}".format(schemaName, column);
            }
            if (number !== columns.length -1) {
                select += "{0}, ".format(column);
            } else {
                select += "{0} ".format(column);
            }
            if (column === "Name") {
				orderby.push(column);
            }
        }, this);
        return "{0} {1} {2} {3}".format(select, from, where, (orderby.length > 0) ? order.format(orderby.join(", ")) : "");
    };

    var parceInsertConfig = function(columnsConfig, schemaName) {
        var insert = "insert into {0} ".format(schemaName);
        var columns = "(Id, ";
        var values = "values (UUId(), ";
        columnsConfig.forEach(function(column, number) {
            if (number !== columnsConfig.length -1) {
                columns += "{0}, ".format(column.columnName);
                values += "'{0}', ".format(column.value);
            } else {
                columns += "{0}) ".format(column.columnName);
                values += "'{0}') ".format(column.value);
            }
        }, this);
        return "{0} {1} {2}".format(insert, columns, values);
    };

    var parceDeleteConfig = function(schemaName, filters) {
        var deleteOp = "delete from {0} ".format(schemaName);
        var where = parceWhereSection(filters);
        return "{0} {1}".format(deleteOp, where);
    };

    var parceUpdateConfig = function(columns, schemaName, filters) {
        var update = "update {0} ".format(schemaName);
        var setOp  = "set ";
        var where = parceWhereSection(filters);
        if (typeof columns === "object") {
            var schema = entitySchemas[schemaName];

            columns.forEach(function(column, number) {
                var col = schema.columns[column.columnName];
                if (number !== columns.length -1) {
                    if (col.Type === "checkbox") {
                        setOp += "{0} = {1}, ".format(column.columnName, column.value);
                    } else {
                        setOp += "{0} = '{1}', ".format(column.columnName, column.value);
                    }
                } else {
                    if (col.Type === "checkbox") {
                        setOp += "{0} = {1} ".format(column.columnName, column.value);
                    } else {
                        setOp += "{0} = '{1}' ".format(column.columnName, column.value);
                    }
                }
            }, this);
        } else {
            return;
        }
        return "{0} {1} {2}".format(update, setOp, where);
    };

    var selectOp = function(config, callback, scope) {
        checkEntityExists(config.schemaName);

        var query = parceSelectConfig(config.columns, config.schemaName, config.filters, config.order);
        console.log("QUERY!! Date: %s, Query: %s", Date(Date.now()), query);
        sqlModel.query(query, function(err, result, fields) {
            if(callback){
                callback.call(scope, err, result, fields);
            }
        });
    };

    var insertOp = function(config, callback, scope) {
        checkEntityExists(config.schemaName);

        var query = parceInsertConfig(config.columns, config.schemaName);
        console.log("QUERY!! Date: %s, Query: %s", Date(Date.now()), query);
        sqlModel.query(query, function(err, result, fields) {
            if(callback){
                callback.call(scope, err, result, fields);
            }
        });
    };

    var deleteOp = function(config, callback, scope) {
        checkEntityExists(config.schemaName);

        var query = parceDeleteConfig(config.schemaName, config.filters);
        console.log("QUERY!! Date: %s, Query: %s", Date(Date.now()), query);
        sqlModel.query(query, function(err, result, fields) {
            if(callback){
                callback.call(scope, err, result, fields);
            }
        });
    };

    var updateOp = function(config, callback, scope) {
        checkEntityExists(config.schemaName);

        var query = parceUpdateConfig(config.columns, config.schemaName, config.filters);
        console.log("QUERY!! Date: %s, Query: %s", Date(Date.now()), query);
        sqlModel.query(query, function(err, result, fields) {
            if(callback){
                callback.call(scope, err, result, fields);
            }
        });
    };

    var checkEntityExists = function (entityName) {
        if (!entitySchemas[entityName]) {
            throw new Error("Unknown entity name: {0}".format(entityName));
        }
    };

    var getEntity = function(config, callback, scope) {
		callback.call(scope, null, entitySchemas[config.entityName]);
    };
    return {
        select: selectOp,
        insert: insertOp,
        delete: deleteOp,
        update: updateOp,
        query: sqlModel.query,
        getEntity: getEntity
    }
};
