var fs = require('fs');

exports.documents = function () {
    var entitySchemas = {};

    var files = fs.readdirSync('./server/models/Documents');
    console.log("-------------------------------------------");
    console.log("Initialize mysql esq models: ");
    console.log("-------------------------------------------");

    files.forEach(function(item) {
        if (item === "index.js" || item === ".git") {
            return
        }

        console.log("\t" + item);
        var modelName = item.substring(0, item.length - 5);
        entitySchemas[modelName] = require("./" + item);
    }, this);

    return entitySchemas;
};
