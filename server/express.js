var express = require('express');
var app = express();

app.set('views', "./app/angularjs/html/");
app.set('view engine', 'pug');

require("./transports").transport(app);
require("./middlewares").middlewares(app, express);
require('./routes').routes(app);

module.exports = app;
