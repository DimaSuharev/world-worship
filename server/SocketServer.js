var io = require('socket.io');

module.exports = function (server) {
    io(server).on('connection', function (socket) {
        socket.emit('chat', 'hello world');
        socket.on('chat', function (data) {
            console.log(data);
            io.sockets.emit('chat', data);
        });
    });
};
