var dataServiceHandler = function (res, req, config, app) {
    var operation = config.operation || "select";
    switch (operation) {
        case "select":
            app.esq.select(config, function(err, rows) {
                if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                res.send(rows);
            }, this);
            break;
        case "insert":
            if (!req.session.CanEdit) {
                res.send(403);
            }

            if (["Event", "User", "Song"].includes(config.schemaName) && req.session.ChurchId) {
                config.columns.push({
                    columnName: "ChurchId",
                    value: req.session.ChurchId
                });
            }

            app.esq.insert(config, function(err, rows) {
                if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                res.send(rows);
            }, this);
            break;
        case "delete":
            if (!req.session.CanEdit) {
                res.send(403);
            }
            app.esq.delete(config, function(err, rows) {
                if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                res.send(rows);
            }, this);
            break;
        case "update":
            if (!req.session.CanEdit) {
                res.send(403);
            }
            app.esq.update(config, function(err, rows) {
                if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                app.setNewHash();
                res.send(rows);
            }, this);
            break;
        case "getEntity":
            app.esq.getEntity(config, function(err, entitySchema) {
                if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                res.send(entitySchema);
            }, this);
            break;
        case "skip":
            res.send(false);
            break;
        case "batch":
            try {
                var index = 0;
                var result = {
                    items: {},
                    send: function (data) {
                        if (data) {
                            this.items[config.items[index].name] = data;
                        }
                        index++;
                        chain();
                    }
                };

                var fns = [];
                config.items.forEach(function (item) {
                    if (!item || !item.name || !item.config) {
                        fns.push({name: dataServiceHandler, args: [result, req, {operation: "skip"}]});
                    }

                    fns.push({name: dataServiceHandler, args: [result, req, item.config, app]});
                }, this);

                var chain = function () {
                    if (fns.length > index) {
                        var args = fns[index].args;
                        fns[index].name.apply({}, args);

                    } else {
                        res.send(result.items);
                    }
                };

                chain();
            } catch (e) {
                res.send(e.message);
            }

            break;
    }
};

exports.routes = function (app) {
    app.post("/DataService", function(req, res) {
        var config = req.body;

        dataServiceHandler(res, req, config, app);
    });
};
