exports.routes = function(app) {

   app.post("/Login", function(req, res) {
       var param = req.body;

       if (!param.userId || !param.userPass) {
           res.send(false);
       }

       app.esq.select({
           columns: ["Id", "Name", "ChurchId", "CanEdit"],
           schemaName: "User",
           filters: [
               {columnName: "Login", value: param.userId},
               {columnName: "Password", value: param.userPass}
           ]
       }, function(err, rows) {
           if (err) throw err;

           var user = rows[0];
           if (!user) {
               res.send(false);
           } else {
               req.session.Id = user.Id;
               req.session.CanEdit = user.CanEdit;
               req.session.Name = user.Name;
               req.session.ChurchId = user.ChurchId;

               app.esq.select({
                   columns: ["*"],
                   schemaName: "FavoriteSong",
                   filters: [{columnName: "UserId", value: req.session.Id}]
               }, function (err, rows) {
                   if (err) throw err;
                   user.favorite = rows;
                   res.send(user);
               });
           }
       });
   });

   app.post("/UserEdit", function (req, res) {
       var param = req.body;
       if (param.Id !== req.session.Id && !req.session.CanEdit) {
           res.send(500);
       }

       app.esq.update({
           columns: [
               {columnName: "Name", value: param.Name},
               {columnName: "Login", value: param.Login},
               {columnName: "Password", value: param.Password},
               {columnName: "Email", value: param.Email},
               {columnName: "Phone", value: param.Phone},
               {columnName: "DefaultRoleId", value: param.DefaultRoleId},
               {columnName: "PhotoUrl", value: param.PhotoUrl}
           ],
           schemaName: "User",
           filters: [{columnName: "Id", value: param.Id}]
       }, function (err, rows) {
           if (err) {
               res.status(403).send(err);
               throw err;
           }
           app.esq.select({
               columns: ["*"],
               schemaName: "User",
               filters: [{columnName: "Id", value: param.Id}]
           }, function (err, users) {
               if (err) throw err;
               app.esq.select({
                   columns: ["*"],
                   schemaName: "FavoriteSong",
                   filters: [{columnName: "UserId", value: req.session.Id}]
               }, function (err, rows) {
                   if (err) throw err;
                   users[0].favorite = rows;
                   res.send(users[0]);
               });
           });

       })
   });
};
