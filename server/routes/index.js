let config = require("../../app/config.json");

exports.routes = function (app) {
    console.log("-------------------------------------------");
    console.log("Initialize routes:");
    console.log("-------------------------------------------");

    let appConfig = config[config.project];
    appConfig.routes.forEach(function (route) {
        require("./" + route).routes(app);
        console.log("\t" + route);
    });
};
