var fs = require('fs');

exports.routes = function(app) {
	var status = "prod";
	if (typeof process.env.OPENSHIFT_NODEJS_IP === "undefined") {
		status = "dev";
	}

	var mainJs = "";

	// status = "prod";

	var jsModules = fs.readdirSync('./app/angularjs/js/');

	app.get('/js/main.js', function(req, res) {
		var script = "angular.module(\"skiniaWorshipApp\", [\"ngCookies\"])\r#scripts#\r;";
		prepareModules(function (code) {
			code += generateSections();
			res.send(script.replace("#scripts#", code));
		});
	});

	var prepareModules = function (callback) {
        if (mainJs !== "") {
            callback.call(scope, mainJs);
            return;
        }
		var modules = "";
		jsModules.forEach(function (module) {
			prepareModule(module, function (script) {
				modules += "{0}\r".format(script);
			})
		}, this);
        console.log("Save main.js");
		callback.call(this, modules);
	};

	var prepareModule = function (moduleName, callback, scope) {
		getScripts(moduleName, function(items) {
			var scripts = "";
			items.forEach(function (item) {
				var script = fs.readFileSync('./app/angularjs/js/{0}/{1}'.format(moduleName, item));
				script = script.toString();
				script = script.substring(script.indexOf("\n"), script.length);
				script = script.substring(0, script.lastIndexOf("\n"));
				scripts += script;
			}, this);
			callback.call(scope, scripts);
		}, this);
	};

	var getScripts = function(moduleName, callback, scope) {
		var items = fs.readdirSync('./app/angularjs/js/' + moduleName + '/');
		callback.call(scope || this, items);
	};

	var generateSections = function () {
		var sections = fs.readdirSync('./app/angularjs/json/sections/');
		var scripts = "";
		sections.forEach(function(name) {
			scripts += generateSection(name);
		});
		return scripts;
	};

	var generateSection = function (name) {
		var json = JSON.parse(fs.readFileSync('./app/angularjs/json/sections/{0}'.format(name)));
		if (json.extendJS) {
			return "";
		}
		var sectionCtr = name.replace(".json", "") + "SectionCtrl";
		var template = "\r\t.controller(\"#sectionCtr#\", function ($scope, $compile, AttributeService, DataService) {\r\t\t" +
			"$scope.$parent.name = \"#sectionName#\";\r\t\t" +
			"var sectionBody = \"#sectionBody#\";\r\t\t" +
			"if (sectionBody) {" +
			"var queryResult = AttributeService.getElement('.section-body');\r\t\t\t" +
			"var wrappedQueryResult = angular.element(queryResult);\r\t\t\t" +
			"var compiled = $compile(sectionBody)($scope);\r\t\t\t" +
			"wrappedQueryResult.append(compiled);\r\t\t\t" +
			"}\r\t\t" +
			"$scope.$parent[\"common-detail\"] = #detail#;\r\t" +
			"})";
		template = template.replace("#sectionCtr#", sectionCtr);
		template = template.replace("#sectionName#", json.sectionName);
        template = template.replace("#sectionBody#", json.html || "");
		template = template.replace("#detail#", JSON.stringify(json.detail));
		return template;
	};
};
