exports.routes = function(app) {

    var processReques = function(config, res) {
        res.setHeader('Content-Type', 'text/html');
        var re = /[A-Z][a-z]*/g;
        var machs = config.pageName.match(re);
        if (machs && machs.indexOf("Section") > -1) {
            var sectionConfig = JSON.parse(fs.readFileSync('./app/angularjs/json/sections/{0}.json'.format(config.pageName.replace("Section", ""))));
            var conf = {
                controller: config.pageName + "Ctrl",
                mode: sectionConfig.mode
            };
            config.pageName = "./template/section";
        }
        res.render(config.pageName, {
            og: config.og || {
                img: "ivents-main",
                title: "World worship teem",
                description: "Church worship teem site",
                url: "http://www.world-worship.com/MainSection"
            },
            hash: app.hash,
            menu: JSON.parse(fs.readFileSync('./app/angularjs/json/Menu.json').toString()),
            record: config.record,
            config: conf
        });
    };

    var fs = require("fs");
    var pug = require('pug');
    pug.renderFile = function(path, options, fn){
        // support callback API
        if ('function' == typeof options) {
            fn = options, options = undefined;
        }
        if (typeof fn === 'function') {
            var res;
            try {
                res = pug.renderFile(path, options);
            } catch (ex) {
                return fn(ex);
            }
            return fn(null, res);
        }

        options = options || {};

        options.filename = path;
        var templ = pug.handleTemplateCache(options);
        //----------------------SCSS----------------------//
        var sass = require('node-sass');

        var getStyles = function (path) {
            if (path.indexOf("index.pug") > -1)
                try {
                    style = sass.renderSync({
                        file: path.replace("index.pug", "/style.scss")
                    });
                    return style.css.toString();
                }
                catch (e) {

                }
        };
        options.style = getStyles(path) || "";

        templ.dependencies.forEach(function (dPath) {
            options.style += getStyles(dPath) || "";
        });
        //----------------------SCSS----------------------//
        return templ(options);
    };

    pug.handleTemplateCache = function (options, str) {
        var key = options.filename;
        if (options.cache && pug.cache[key]) {
            return pug.cache[key];
        } else {
            if (str === undefined) {
                str = fs.readFileSync(options.filename, 'utf8');

            }
            var templ = pug.compile(str, options);
            if (options.cache) pug.cache[key] = templ;
            return templ;
        }
    };

    var renderComponent = function (comName) {
        return pug.renderFile("./angularjs/html/components/" + comName + "/index.pug", {
            menu: JSON.parse(fs.readFileSync('./angularjs/json/Menu.json').toString())
        });
    };

    app.get("/new/:name", function (req, res, next) {
        res.send(renderComponent(req.params.name));
    });

    app.get('/', function(req, res, next) {
        processReques({pageName: "MainSection"}, res);
    });

    app.get('/index.html.var', function(req, res, next) {
        processReques({pageName: "MainSection"}, res);
    });

    app.get('/modalBox/:name', function(req, res, next) {
        res.send(pug.renderFile("./app/angularjs/html/modalBox/{0}.pug".format(req.params.name), {}));
    });

    app.get('/:pageName', function(req, res, next) {
        processReques({pageName: req.params.pageName}, res);
    });

    app.get('/:pageName/:id', function(req, res, next) {
        var re = /[A-Z][a-z]*/;
        var entityName = req.params.pageName.match(re)[0];
        app.esq.select({
            columns: ["*"],
            schemaName: entityName,
            filters: [{columnName: "Id", value: req.params.id}]
        }, function(err, rows) {
            if (err) throw err;
            var row = rows[0];
            if (!row) {
                res.send("Error");
                return;
            }
            row.entityName = entityName;
            processReques({
                og: {
                    img: row.ImageUrl || "http://songs-skiniiaworship.rhcloud.com/images/logo.jpg",
                    title: row.Name || "No name",
                    description: row.Body || "No body"
                },
                pageName: req.params.pageName,
                record: row
            }, res);
        }, this);

    });
};
