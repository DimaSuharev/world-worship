exports.routes = function(app) {

    app.post('/songId=:name', function(req, res) {
        var handleValue = function (value) {
            return value
                ? value.replace("'", "\"")
                : "";
        };

        var songId = req.params.name;
        var song = req.body;
        var songUdateQuery = 'update Song \n' +
            "set YouTubeUri = '" + song.youtubeUri + "',\n" +
            "   AudioLink = '" + song.AudioLink + "',\n" +
            "   Name = '" +  handleValue(song.Name) + "',\n" +
			"   Temp = " + song.temp + ",\n" +
			"   Multitracks = '" + song.multitracks + "',\n" +
			"   Body = '" + handleValue(song.Body) + "',\n" +
			"   `Order` = '" + song.order + "'\n" +
            "where Id = '" + songId + "'";

        app.esq.query(songUdateQuery, function(err) {
            if (err) throw err;
        });

        var parts = song.parts;
        for(var i in parts) {
            var part = parts[i];
            if (part.id) {
                var partUpdateQuery = 'update SongPart \n' +
                    "set Name = '" + handleValue(part.name) + "',\n" +
                    "Text = '" + handleValue(part.text) + "',\n" +
                    "Chords = '" +  handleValue(part.chords) + "',\n" +
					"BassChords = '" +  handleValue(part.bassChords) + "'\n" +
                    "where Id = '" + part.id + "'";
				// res.send(partUpdateQuery);
                app.esq.query(partUpdateQuery, function(err) {
                    if (err) throw err;
                });
            } else {
                var partInsertQuery = 'insert into SongPart \n' +
                    '(Id, Name, Text, Chords, BassChords, Position, SongId) \n' +
                    "values (UUID(), \n" +
                    "'" +  handleValue(part.name) + "', \n" +
                    "'" +  handleValue(part.text) + "', \n" +
                    "'" +  handleValue(part.chords) + "', \n" +
					"'" +  handleValue(part.bassChords) + "', \n" +
                    "'" + i + "', \n" +
                    "'" + songId + "') \n";

                app.esq.query(partInsertQuery, function(err) {
                    if (err) throw err;
                });
            }

        }
        app.setNewHash();
        res.send("true");
    });

    app.get('/songId=:name', function(req, res) {
        var song = {
            Id: "",
            Name: "",
            youtubeUri: "",
            AudioLink: "",
            temp: "",
			multitracks: "",
			Body: "",
            order: "",
            parts: []
        };
        var songId = req.params.name;
        var query =
            "select s.Id, s.Name as SongName, s.Body as Body, s.Order as `Order`, s.Temp as Temp, s.Multitracks as Multitracks, " +
            "s.YouTubeUri, s.AudioLink, p.Id as PartId, p.Name as PartName, p.Chords, p.BassChords, p.Text from Song s " +
            "	left join SongPart p on p.SongId = s.Id " +
            "where s.Id ='" + songId + "' " +
            "order by p.`Position`";

        app.esq.query(query, function(err, rows, fields) {
            if (err) throw err;
            rows.forEach(function(row){
                if (!song.Id) {
                    song.Id = row.Id;
                    song.Name = row.SongName;
                    song.youtubeUri = row.YouTubeUri;
                    song.AudioLink = row.AudioLink;
                    song.order = row.Order;
                    song.temp = row.Temp;
                    song.multitracks = row.Multitracks;
                    song.Body = row.Body
                }
                // if (row.PartName) {
                    song.parts.push({
                            id: row.PartId,
                            name: row.PartName,
                            text: row.Text,
                            chords: row.Chords,
                            bassChords: row.BassChords
                        }
                    );
                // }
            });
            res.setHeader('Content-Type', 'application/json; charset=utf-8');
            res.send(JSON.stringify(song));
        });
    });

    app.post('/SearchSongByName', function(req, res) {
        var params = req.body;
        var query = "select distinct s.Id, s.Name, s.YouTubeUri " +
            "from Song s left join SongPart p on p.SongId = s.Id " +
            "where s.Name like '%" + params.songName + "%' " +
            "or p.Text like '%" + params.songName + "%' order by s.Name";

        app.esq.query(query, function(err, rows) {
            if (err) throw err;
            res.send(rows);
        });
    });

    app.post('/LastActivity', function (req, res)  {
        var songQuery = "select\n" +
            "\t Song.Id, Song.Name, Song.YouTubeUri as YouTubeUri, count(Song.Id)\n" +
            "from Song \n" +
            "\tleft join SongInIvent on SongInIvent.SongId = Song.Id\n" +
            "\tleft join Event on Event.Id = SongInIvent.IventId\n" +
            "where Event.Date BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW()\n" +
            "group by Song.Id, Song.Name\n" +
            "order by 3 desc\n" +
            "LIMIT 5";

/*
select Song.Id, Song.Name, Song.YouTubeUri as YouTubeUri, count(Song.Id)
from Song
left join SongInIvent on SongInIvent.SongId = Song.Id
left join Ivent on Ivent.Id = SongInIvent.IventId
where Ivent.Date BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW()
group by Song.Id, Song.Name
order by 3 desc LIMIT 5
 */

        var eventQuery = "select * from Event where Date > DATE_ADD(NOW(), INTERVAL -1 DAY) order by Date";
        app.esq.query(songQuery, function(err, songRows) {
            if (err) throw err;
            app.esq.query(eventQuery, function(err, eventsRows) {
                if (err) throw err;
                if (req.session.Id) {
                    app.esq.select({
                        columns: ["*"],
                        schemaName: "FavoriteSong",
                        filters: [{columnName: "UserId", value: req.session.Id}]
                    }, function (err, rows) {
                        if (err) throw err;
                        res.send({
                            songs: songRows,
                            events: eventsRows,
                            favorite: rows
                        });
                    });
                } else {
                    res.send({
                        songs: songRows,
                        events: eventsRows
                    });
                }
            });
        });
    });

    app.post('/Schedule', function (req, res) {
        var scheduleQuery = "select Event.Id, Event.Name, Event.ImageUrl, `User`.`Name` as UserName, UserInIventRole.Id as `Exists` from `User`\n" +
            "\tcross join Event\n" +
            "\tleft join UserInIventRole on UserInIventRole.IventId = Event.Id and `User`.Id = UserInIventRole.UserId\n" +
            "WHERE Event.Date > NOW()\n" +
            "\tand `User`.`Active` = 1\n" +
            "ORDER by Event.Date";

        app.esq.query(scheduleQuery, function(err, scheduleRows) {
            if (err) throw err;
            res.send(scheduleRows);
        });
    });

    app.post("/FavoriteSong", function (req, res) {
        var params = req.body;

        if (!params.SongId || !params.UserId) {
            console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), "FavoriteSong no SongId or UserId");
            res.send("");
            return;
        }

        var schema = "FavoriteSong";

        app.esq.select({
            columns: [
                "Id"
            ],
            schemaName: schema,
            filters: [
                {
                    columnName: "SongId",
                    value: params.SongId
                },
                {
                    columnName: "UserId",
                    value: params.UserId
                }
            ]
        }, function (err, rows) {
            if (err) throw err;
            if (rows.length === 0) {
                app.esq.insert({
                    columns: [
                        {columnName: "SongId", value: params.SongId},
                        {columnName: "UserId", value: params.UserId}
                    ],
                    schemaName: schema
                }, function(err) {
                    if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                    res.send(true);
                }, this);
            } else {
                app.esq.delete({
                    schemaName: schema,
                    filters: [
                        {columnName: "SongId", value: params.SongId},
                        {columnName: "UserId", value: params.UserId}
                    ]
                }, function (err) {
                    if (err) console.log("ERROR!! Date: %s, Code : %s", Date(Date.now()), err.code);
                    res.send(false);
                })
            }
        });
    });
};
