var fs = require('fs');

exports.routes = function(app) {

	app.get('/Menu.json', function(req, res) {
		res.setHeader('Content-Type', 'application/json');
		res.send(fs.readFileSync('./app/public/json/Menu.json') );
	});

	app.get("/JsonSections", function(req, res) {
		var sections = fs.readdirSync('./app/public/json/sections/');
		var config = [];
		sections.forEach(function(name) {
			config.push({
				"Name": name.replace(".json", "Section"),
				"ImageUrl": "",
				"GoTo": "/JsonPage/" + name
			})
		});
		res.send(config);
	});

	app.get("/JsonObjects", function(req, res) {
		var sections = fs.readdirSync('./app/server/modules/esqModels/');
		var config = [];
		sections.forEach(function(name) {
			config.push({
				"Name": name.replace(".json", ""),
				"ImageUrl": "",
				"GoTo": "/JsonObject/" + name
			})
		});
		res.send(config);
	});

	app.get("/JsonPage/:name", function(req, res) {
		res.setHeader('Content-Type', 'text/html');
		res.render("JsonPage", {
			og: {
				img: "ivents-main",
				title: "asd",
				description: "asd",
				url: "aaaa"
			},
			menu: JSON.parse(fs.readFileSync('./app/public/json/Menu.json').toString()),
			record: {
				Id: req.params.name,
				entityName: req.params.name.replace(".json", "Section")
			}
		});
	});

	app.post("/SaveJson/:name", function (req, res) {
        app.setNewHash();
		fs.writeFile("./app/public/json/sections/" + req.params.name, JSON.stringify(req.body), function () {
			res.send("Успешно");
		});
	});
};