exports.routes = function (app) {
    app.post("/SendEmail", function(req, res) {
        app.esq.select({
            columns: ["UserId.Email", "IventId.Name"],
            schemaName: "UserInIvent",
            filters: [{"columnName": "IventId", "value": req.body.iventId}]
        }, function(err, rows) {
            if (err) throw err;
            var collection = [];
            rows.forEach(function(item) {
                if (item["UserId.Email"] !== "" && collection.findIndex(function (item1) {
                    return item1 === item["UserId.Email"];
                }) === -1)  {
                    collection.push(item["UserId.Email"]);
                }
            }, this);
            var mailOptions = {
                from: '"Worship Skinia" <worship.skinia@gmail.com>',
                to: collection.join(","),
                subject: rows[0]["IventId.Name"],
                text: 'Напоминаю что репетиция будет перед собранием в 13:30, Крещатик 7/11'
            };
            app.mailer.sendMail(mailOptions, function(error, info) {
                if (error) {
                    res.send(error);
                }
                res.send("Успешно отправленно");
            });
            // res.send(collection);
        }, this);

    });
};
