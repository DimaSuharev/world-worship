var config = require("../../app/config.json");

exports.middleware = function (app) {
    app.hash = config.hash;

    app.setNewHash = function () {
        var date = new Date();
        var month = '' + (date.getMonth() + 1),
            day = '' + date.getDate(),
            year = date.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        this.hash = [year, month, day, h, m, s].join("");
    }.bind(app);

    app.use(function (req, res, next) {
        var path = req._parsedUrl.pathname;

        if (req.method === "GET") {
            if (config.project === "vue") {
                var newPath = "";
                if (path.startsWith("/MainSection")) {
                    newPath = "/";
                } else if (path.startsWith("/SchedulerPage")) {
                    newPath = "/ScheduleSection";
                } else if (path.startsWith("/IventSection")) {
                    newPath = "/EventSection";
                } else if (path.startsWith("/IventPage")) {
                    newPath = path.replace("IventPage", "EventPage");
                } else if (path = "/m/js/app.bundles.min.js") {
                    next();
                    return;
                }

                if (newPath) {
                    res.redirect(config.host[config.mode] + newPath);
                    return;
                }
            } else {
                if (req._parsedUrl.query !== app.hash) {
                    res.redirect(path + "?" + app.hash);
                    return;
                }
            }
        }
        next();
    });
};
