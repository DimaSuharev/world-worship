exports.middleware = function (app) {
    app.use(function(req, res, next) {
        var urls = ['/SearchSongByName', '/Login', '/LastActivity', '/Schedule', '/EditUser'];
        if (req.session.CanEdit || urls.includes(req.url) || req.method === "GET") {
            next();
        }  else if (req.url === '/DataService') {
            next();
        } else {
            res.status(403).send("url: " + req.url + "; CanEdit: " + req.session.CanEdit);
        }
    });
};
