var compression = require('compression');

exports.middleware = function (app) {
    var shouldCompress = function(req, res) {
        if (req.headers['x-no-compression']) {
            return false
        }
        return compression.filter(req, res)
    };

    app.use(compression({filter: shouldCompress}));
};
