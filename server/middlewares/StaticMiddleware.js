var config = require("../../app/config.json");

exports.middleware = function (app, express) {
    app.use(express.static('web/assets/' + config.project));
};
