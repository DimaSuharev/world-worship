exports.middleware = function (app) {
    app.use(function(req, res, next) {
        if (!res.getHeader('Cache-Control') && req.url !== '/DataService') {
            res.setHeader('Cache-Control', 'public, max-age=7200');
        }
        next();
    });
};
