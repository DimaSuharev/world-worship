var bodyParser = require("body-parser");

exports.middleware = function (app) {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
};
