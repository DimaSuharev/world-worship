var cookieSession = require('cookie-session');
var cookieParser = require('cookie-parser');

exports.middleware = function (app) {
    app.use(cookieSession({
        name: 'session',
        signed: false,
        httpOnly: false
    }));
    app.use(cookieParser());
    app.use(function (req, res, next) {
        req.session.views = (req.session.views || 0) + 1;
        next();
    });
};
