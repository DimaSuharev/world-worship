var config = require("../../app/config.json");
let fs = require("fs");

exports.middlewares = function (app, express) {
    console.log("-------------------------------------------");
    console.log("Initialize middlewares:");
    console.log("-------------------------------------------");

    app.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    var appConfig = config[config.project];
    app.get("/js/app.js", (req, res) => {
        const file = fs.readFileSync(`./web/assets/vue/js/app.js`);
        res.setHeader("Content-Type", "application/javascript");
        res.send(file);
    });

    appConfig.middlewares.forEach(function (middleware) {
        require("./" + middleware).middleware(app, express);
        console.log("\t" + middleware);
    });
};
