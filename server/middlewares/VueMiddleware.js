let path = require('path');

exports.middleware = function (app) {
    var og = {
        title: "Сайт группы прославления Skinia Worship",
        description: "",
        img: ""
    };

    var getEsqConfig = function (path) {
        var parts = path.split("/");
        var guidRX = /[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}/;
        var config;

        if (parts.length > 2) {
            var id = parts[2].match(guidRX);
            if (id && id.length > 0) {
                switch (parts[1]) {
                    case "SongPage":
                        config = {
                            columns: ["Name", "Body", "YouTubeUri"],
                            schemaName: "Song",
                            filters: [{columnName: "Id", value: id[0]}]
                        };
                        break;
                    case "EventPage":
                        config = {
                            columns: ["Name", "Body", "ImageUrl"],
                            schemaName: "Event",
                            filters: [{columnName: "Id", value: id[0]}]
                        };
                        break;
                }
            }
        }

        return config;
    };

    app.use(function (req, res, next) {
        if (req.method === "GET") {
            var indexPath = path.resolve(__dirname, '../../app/vue/app/index');
            var esqConfig = getEsqConfig(req._parsedUrl.pathname);
            if (esqConfig) {
                app.esq.select(esqConfig, function (err, rows) {
                    if (err) throw err;
                    if (rows.length > 0) {
                        var entity = rows[0];

                        if (entity) {
                            switch (esqConfig.schemaName) {
                                case "Song":
                                    let search = entity.YouTubeUri && entity.YouTubeUri.match(/[^=]+$/) || [];
                                    var img = "";
                                    if (search.length > 0) {
                                        img = "https://img.youtube.com/vi/" + search[0] + "/hqdefault.jpg";
                                    }
                                    og = {
                                        title: entity.Name,
                                        description: entity.Body && entity.Body.replace(/(<p>|<\/p>|<strong>[^<>]*<\/strong>|<i>[^<>]*<\/i>|<br>)/g, " ").replace(/[ ]{2,20}/g, " ").trim(),
                                        img: img
                                    };
                                    break;
                                case "Event":
                                    og = {
                                        title: entity.Name,
                                        description: entity.Body && entity.Body.replace(/(<p>|<\/p>|<strong>|<\/strong>|<i>|<\/i>|<br>)/g, " ").trim(),
                                        img: entity.ImageUrl
                                    };
                                    break;
                            }
                        }
                    }
                    res.render(indexPath, {og: og});
                });
            } else {
                res.render(indexPath, {og: og || {}});
            }
            return
        }
        next();
    });
};
