var format = require('string-format');
format.extend(String.prototype);

var config = require("../app/config");

var NodeApp = function() {

    //  Scope.
    var self = this;

    /**
     *  Initializes the sample application.
     */
    self.init = function() {
        self.initTerminationHandlers();
        self.initServer();

        return self;
    };

    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
    self.terminator = function(sig){
        if (typeof sig === "string") {
           console.log('%s: Received %s - terminating sample app ...',
                       Date(Date.now()), sig);
           process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()) );
    };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
    self.initTerminationHandlers = function(){
        process.on('exit', function() {
            self.terminator();
        });
    };

    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initServer = function() {
        var app = require("./express");
        self.app = app;
        app.connection = require('./models')(app);
    };

    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        var server =  self.app.listen(config.port, config.ipaddress, function() {
            console.log("-------------------------------------------");
            console.log('%s: Node server started on %s:%d ...', new Date(), config.ipaddress, config.port);
            console.log("-------------------------------------------");
        });
        require('./SocketServer')(server);
    };
};

new NodeApp().init().start();
