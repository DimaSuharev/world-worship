CREATE TABLE `Song` (
  `Id` char(36) NOT NULL,
  `Name` varchar(250) CHARACTER SET utf8 DEFAULT '',
  `YouTubeUri` varchar(250) CHARACTER SET utf8 DEFAULT '',
  PRIMARY KEY (`Id`)
);

alter table songs.`Song` add column GenreId char(36);
alter table songs.`Song` add column AuthorId char(36);

alter table songs.`Song` add column Temp int;
alter table songs.`Song` add column `Order` varchar(1500);
alter table songs.`Song` add column `Multitracks` varchar(250);
alter table songs.`Song` add column `AudioLink` varchar(250);

alter table worldwor_songs.`Song` add column ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table worldwor_songs.`Song` add column Body varchar(10000);

alter table worldwor_songs.`Song` add column CreatedOn datetime DEFAULT CURRENT_TIMESTAMP;

alter table worldwor_songs.`Song` add column ChurchId char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
