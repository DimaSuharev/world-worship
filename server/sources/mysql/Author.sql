CREATE TABLE songs.`Author` (
    `Id`  char(36) NOT NULL,
    `Name`  char(250) NOT NULL,
    `ImageUrl`  nvarchar(250) NULL Default '',
    PRIMARY KEY(Id)
  );