CREATE TABLE songs.`SongPart` (
    Id  char(36) NOT NULL,
    SongId  char(36) NOT NULL,
    Chords  nvarchar(490) NULL Default '',
    `Text`  nvarchar(490) NULL Default '',
    `Position` int NULL Default 0,
    PRIMARY KEY(Id),
    FOREIGN KEY (SongId) REFERENCES songs.`Song`(Id)
  ) 

alter table songs.SongPart add BassChords nvarchar(490) DEFAULT '';
alter table songs.SongPart add Name nvarchar(250) DEFAULT '';