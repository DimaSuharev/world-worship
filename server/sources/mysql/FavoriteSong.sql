CREATE TABLE `FavoriteSong` (
    Id  char(36) NOT NULL,
    SongId  char(36) NOT NULL,
    UserId  char(36) NOT NULL,
    PRIMARY KEY(Id),
    FOREIGN KEY (SongId) REFERENCES `Song`(Id),
    FOREIGN KEY (UserId) REFERENCES `User`(Id)
  )
