CREATE TABLE worldwor_songs.`User` (
  `Id` char(36) not null,
  `Name` nvarchar(250) not null DEFAULT '',
  `Login` nvarchar(250) not null DEFAULT '',
  `Password` nvarchar(250) not null DEFAULT '',
  `Email` nvarchar(250) DEFAULT '',
  `CanEdit` bit default 0,
  `Phone` nvarchar(250) DEFAULT '',
  `DefaultRoleId` char(36),
  `Active` bit default 1,
  `ModifiedOn` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PhotoUrl`  nvarchar(250) NULL Default '',
  ChurchId char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`DefaultRoleId`) REFERENCES worldwor_songs.`UserRole`(Id)
);
