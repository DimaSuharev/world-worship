CREATE TABLE worldwor_songs.`Church` (
    `Id`  char(36) NOT NULL,
    `Name`  char(250) NOT NULL,
    PRIMARY KEY(Id)
  );

alter table worldwor_songs.`Church` add column `MainImageUrl`  nvarchar(250) NULL;
alter table worldwor_songs.`Church` add column `LogoUrl` nvarchar(250) NULL;
alter table worldwor_songs.`Church` add column `Body` varchar(10000);
alter table worldwor_songs.`Church` add column CreatedOn datetime DEFAULT CURRENT_TIMESTAMP;
alter table worldwor_songs.`Church` add column ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
