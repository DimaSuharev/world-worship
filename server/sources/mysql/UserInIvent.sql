CREATE TABLE songs.`UserInIvent` (
    Id  char(36) NOT NULL,
    UserId  char(36) NOT NULL,
    IventId  char(36) NOT NULL,
    RoleId char(36) null,
    PRIMARY KEY(Id),
    FOREIGN KEY (UserId) REFERENCES songs.`User`(Id),
    FOREIGN KEY (IventId) REFERENCES songs.`Ivent`(Id),
    FOREIGN KEY (RoleId) REFERENCES songs.`UserRole`(Id)
  ) 
