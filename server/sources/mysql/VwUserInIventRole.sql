CREATE OR REPLACE VIEW `VwUserInIventRole` AS
select
	uiir.Id,
	uiir.UserId,
	uiir.IventId,
	IFNULL(uiir.RoleId, u.DefaultRoleId) as RoleId,
	IFNULL(ur1.ImageUrl, ur2.ImageUrl) as ImageUrl,
	IFNULL(ur1.Position, ur2.Position) as Position
from UserInIventRole uiir
	left join User u on u.Id = uiir.UserId
	left join UserRole ur1 on ur1.Id = uiir.RoleId
	left join UserRole ur2 on ur2.Id = u.DefaultRoleId