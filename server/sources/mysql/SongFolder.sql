CREATE TABLE songs.`SongFolder` (
  `Id` char(36) not null,
  `Name` nvarchar(250) DEFAULT '',
  PRIMARY KEY (`Id`)
);

alter table songs.SongFolder add ParentId char(36);
alter table songs.SongFolder add IsNode bit default 0;
alter table songs.SongFolder add ImageUrl nvarchar(250);