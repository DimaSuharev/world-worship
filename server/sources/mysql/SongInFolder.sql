CREATE TABLE songs.`SongInFolder` (
    Id  char(36) NOT NULL,
    SongId  char(36) NOT NULL,
    FolderId  char(36) NOT NULL,
    PRIMARY KEY(Id),
    FOREIGN KEY (SongId) REFERENCES songs.`Song`(Id),
    FOREIGN KEY (FolderId) REFERENCES songs.`SongFolder`(Id)
  ) 
