CREATE TABLE Event (
    Id  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    Name  char(250) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    Body  varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL Default '',
    SongFolderId  char(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL,
    Date datetime NULL,
    UserFolderId char(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL,
    Address varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci ,
    Address2 varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci ,
    ModifiedOn datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CreatedOn datetime DEFAULT CURRENT_TIMESTAMP,
    ChurchId char(36) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL,
    ImageUrl varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL Default 'images/event.jpg',

    PRIMARY KEY(Id),
    FOREIGN KEY (SongFolderId) REFERENCES SongFolder(Id),
    FOREIGN KEY (UserFolderId) REFERENCES UserFolder(Id),
    FOREIGN KEY (ChurchId) REFERENCES Church(Id)
  );
