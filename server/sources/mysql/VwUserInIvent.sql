Create View VwUserInIvent
as
select i.Id, i.IventId, i.UserId, GROUP_CONCAT(r.ImageUrl SEPARATOR ',') as ImageUrl from UserInIventRole u
	join UserRole r on r.Id = u.RoleId
	join `UserInIvent` i on i.UserId = u.`UserId` and i.`IventId` = u.`IventId`
group by i.Id, i.IventId, i.UserId