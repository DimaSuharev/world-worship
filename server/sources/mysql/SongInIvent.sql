CREATE TABLE songs.`SongInIvent` (
    Id  char(36) NOT NULL,
    SongId  char(36) NOT NULL,
    IventId  char(36) NOT NULL,
    PRIMARY KEY(Id),
    FOREIGN KEY (SongId) REFERENCES songs.`Song`(Id),
    FOREIGN KEY (IventId) REFERENCES songs.`Ivent`(Id)
  )

alter table songs.SongInIvent add `Position` int;